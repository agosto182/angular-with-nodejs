import { Component, OnInit } from '@angular/core';
import {UserService} from './services/user.service';
import { User } from './models/user';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService]
})

export class AppComponent implements OnInit{
  public title = 'Musify';
  public user: User;
  public user_register: User;
  public identity;
  public token;
  public errorMsg;
  public alertRegister;

  constructor(
  	private _userService:UserService
  	){
  	this.user=new User('','','','','','ROLE_USER','');
  	this.user_register=new User('','','','','','ROLE_USER','');
  }

  ngOnInit(){
  	this.identity = this._userService.getIdentity();
  	this.token = this._userService.getToken();
  }

  public onSubmit(){
  	//console.log(this.user);

  	this._userService.signup(this.user).subscribe(
  		response=>{
  			var identity=response.user;
  			this.identity=identity;

  			if(!this.identity._id){
  				alert('Ocurrio un error en el loggeo');
  			}
  			else{
  				//Crear localstorage
  				localStorage.setItem('identity',JSON.stringify(this.identity));
  				this._userService.signup(this.user,"true").subscribe(
			  		response=>{
			  			var token=response.token;
			  			this.token=token;

			  			if(this.token<10){
			  				alert('Ocurrio un error en el loggeo');
			  			}
			  			else{
			  				//Crear localstorage
			  				localStorage.setItem('token',this.token);
			  				this.user=new User('','','','','','ROLE_USER','');
			  			}
			  		},
			  		error=>{
			  			var errorMsg= <any>error;
			  			if(errorMsg!=null){
			  				var body=JSON.parse(error._body);
			  				this.errorMsg=body.message;
			  				console.log(errorMsg);
			  			}
			  		}
			  	);
  			}
  		},
  		error=>{
  			var errorMsg= <any>error;
  			if(errorMsg!=null){
  				var body=JSON.parse(error._body);
  				this.errorMsg=body.message;
  				console.log(errorMsg);
  			}
  		}
  	);
  }

  logout(){
  	localStorage.removeItem('token');
  	localStorage.removeItem('identity');
  	localStorage.clear();

  	this.identity=null;
  	this.token=null;
  }

  onSubmitRegister(){
  	//console.log(this.user_register);

  	this._userService.register(this.user_register).subscribe(
  		response=>{
  			let user=response.user;
  			this.user_register=user;

  			if(!user._id){
  				alert("Error al registrarse!");
  			}
  			else{
  				this.alertRegister="El registro fue correcto con "+this.user_register.email+"!";
  				this.user_register=new User('','','','','','ROLE_USER','');
  			}
  		},
  		error=>{
  			var alertRegister= <any>error;
  			if(alertRegister!=null){
  				var body=JSON.parse(error._body);
  				this.alertRegister=body.message;
  				console.log(alertRegister);
  			}
  		}
  		);
  }
}
