'use strict'

var fs = require('fs');
var path = require('path');
var mongoosePaginate= require('mongoose-pagination');
//var Artist = require('../models/artist');
var Album = require('../models/album');
var Song = require('../models/song');

function getSong(req,res){
	var songId=req.params.id;

	Song.findById(songId).populate({path:'album'}).exec((err,song)=>{
		if(err){
			res.status(500).send({message:"Error en el servidor"});
		}
		else if(!song){
			res.status(404).send({message:"Cancion no encontrada"});
		}
		else{
			res.status(200).send({song});
		}
	})
}

function saveSong(req,res){
	var song=new Song();
	var params=req.body;

	if(params.number==null || params.name==null || params.duration==null || params.album==null){
		return res.status(500).send({message:"Faltan datos de cancion."});
	}

	song.number=params.number;
	song.name=params.name;
	song.duration=params.duration;
	song.file="null";
	song.album=params.album;

	song.save((err,songStored)=>{
		if(err || !songStored){
			return res.status(500).send({message:"Error en el servidor."});
		}
		else{
			return res.status(200).send({song:songStored});
		}
	})

}

function getSongs(req,res){
	var albumId=req.params.album;

	if(!albumId){
		var find=Song.find({}).sort('name');
	}
	else{
		var find=Song.find({album:albumId}).sort('number');
	}

	find.populate({
		path:'album',
		populate:{
			path:'artist',
			model:'Artist'
		}
	}).exec(function(err,songs){
		if(err || !songs){
			return res.status(500).send({message:"Error en el servidor."});
		}
		else{
			return res.status(200).send({songs});
		}

	})
}

function updateSong(req,res){
	var songId=req.params.id;
	var update=req.body;

	Song.findByIdAndUpdate(songId,update,(err,songUpdated)=>{
		if(err){
			return res.status(500).send({message:"Error en el servidor."});
		}
		else if(!songUpdated){
			return res.status(404).send({message:"Cancion no encontrada."});
		}
		else{
			return res.status(200).send({song:songUpdated});
		}
	});
}

function deleteSong(req,res){
	var songId=req.params.id;
	
	Song.findByIdAndRemove(songId,(err,songRemoved)=>{
		if(err){
			return res.status(500).send({message:"Ocurrio un error en el servidor."});
		}
		if(songRemoved){
			return res.status(200).send({songRemoved});
		}else{
			return res.status(404).send({message:"No se encontro la cancion."});	
		}
	});
		
}

function uploadFile(req,res){
	var songId = req.params.id;
	var file_name = "No subido..";

	if(req.files.file){
		var file_path = req.files.file.path;
		var file_split = file_path.split('\\');
		file_name = file_split[2];

		var file_ext = file_name.split('\.')[1].toLowerCase();
		if(file_ext=="mp3" || file_ext=="mdi" || file_ext=="ogg"){
			Song.findByIdAndUpdate(songId, {file:file_name}, (err,songUpdated) =>{
				if(err){
					return res.status(500).send({message:"Error en el servidor."});
				}
				if(!songUpdated){
					return res.status(404).send({message:"Cancion no encontrada"});
				}
				else{
					return res.status(200).send({song:songUpdated});
				}
			});
		}
		else{
			return res.status(200).send({message:"No se subio ninguna cancion."});			
		}

		console.log(file_path);
	}else{
		return res.status(200).send({message:"No se subio ninguna cancion."});
	}
}

function getSongFile(req,res){
	var songFile = req.params.songFile;
	var path_file = './uploads/songs/'+songFile;
	fs.exists(path_file, function(exists){
		if(exists){
			res.sendFile(path.resolve(path_file));
		}
		else{
			res.status(200).send({message:"No se encontro ninguna cancion."});
		}
	});
}

module.exports={
	getSong,
	saveSong,
	getSongs,
	updateSong,
	deleteSong,
	uploadFile,
	getSongFile
}